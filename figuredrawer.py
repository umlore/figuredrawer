import asyncio
from collections import defaultdict
import discord
from dotenv import load_dotenv
import glob
import os
import random
import time

#define supported channels here!
timerchanneltitles = {60: "60-sec-draws", 300: "5-min-draws", 900: "warmup-draws"}

#set up client connection
load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')

client = discord.Client()

@client.event
async def on_ready():
	print('We have logged in as {0.user}'.format(client))

"""	interval: time in seconds between posting images
	directory: place where images are
	channel: id of channel to post in """
async def timerchannel(interval, directory, channel):
	print ("timerchannel process spawned for channelid: " + str(channel))
	chan = client.get_channel(channel)

	#get all the images in the image directory
	imagefns = os.listdir("images")
	
	#make sure the client is ready
	await client.wait_until_ready()
	#ready to post immediately once client is ready
	nextpost = time.time()

	while client.is_ready():
		#it's time to post
		if time.time() >= nextpost:
			#post
			with open("images/" + imagefns[random.randint(0, len(imagefns) - 1)], 'rb') as f:
				image = discord.File(f)
				await chan.send(file = image)

			#set the next post time
			nextpost += interval
		else:
			await asyncio.sleep(1)

async def inittimerchannels():
	await client.wait_until_ready()

	#map of post interval (in seconds) to channel IDs of timer channels
	timerchannelids = defaultdict(lambda: [])

	#populate timerchannelids: duration mapped to channelids
	for chan in client.get_all_channels():
		for key in timerchanneltitles:
			if timerchanneltitles[key] in chan.name:
				#if this channel id is supported, add to structure,
				timerchannelids[key].append(chan.id)
				#and spawn a timerchannel thread
				client.loop.create_task(timerchannel(key, "images", chan.id))

client.loop.create_task(inittimerchannels())
client.run(TOKEN)