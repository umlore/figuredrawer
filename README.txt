figure drawer is a discord bot for a channel I run that facilitates timed figure drawing exercises.  It perpetually posts images at set intervals in dedicated channels for 60s, 5min, 15min, etc interval sketch practice.  There's a million improvements that could be made in order to scale this and make it reusable in other discord servers, but it works great for the small club I run.

how to use:
figuredrawer is configured to pull random images from a directory of images called "images".  You'll have to make this directory and populate it yourself, I don't have the rights to distribute them all, some are from group members, etc.

the discord token is read from an environment variable (not committed, for obvious reasons)

to change the configuration, you can edit the timerchanneltitles dict.
the key is the post interval in seconds, the value is the title of the channel.  You have to create the channel yourself.

If you've created the channels & directory, this bot will post automatically for you at the defined interval.  It can maintain several channels at once.